.PHONY: howto

flake8:
	flake8 --doctest assign_reviews.py

pylint:
	pylint assign_reviews.py

pycodestyle:
	pycodestyle assign_reviews.py

pydocstyle:
	pydocstyle assign_reviews.py

doctest:
	python -m doctest -v assign_reviews.py

howto:
	cd howto; pandoc howto.md -o howto.pdf

lint: flake8 pylint pycodestyle pydocstyle
test: doctest
