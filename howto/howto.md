# Exercise-Review workflow using an automated reassignment script

This documents details a workflow for OLAT submissions for review
originally developed for the project "JUNOSOL - Jupyter-notebooks for
self-organized learning" using a helper script to unpack, obfuscate,
shuffle, test, export and pack OLAT submissions for review.

Students submit jupyter notebooks to OLAT exercises, which get
redistributed randomly to their peers and reviewed by them. Read more
of the general workflow below.

## Workflow:

In general, we are trying to implement a submit-review-feedback cycle
in OLAT, which is not supported natively, so we are using the revise
submission function to assign reviewees to reviewers.

0. **Prepare participants file.**

    See example `participants.md`. This file contains email and names
    to contact students with faulty submissions.

1. **Create Exercise for students in OLAT and assign**

    Students will be able to accept assignments and download/work on
    the assigned jupyter notebooks exercise sheets.

1. **Download OLAT generated zip of solutions.**

    Once the exercise deadline expired, OLAT lets you download all
    submissions of an exercise as one (broken…) zip file.
 
    ![](screenshot_download_submissions.png)

2. **Run zip through assign_reviews.py**
 
    Run

        python assign_reviews.py submissions.zip target_folder

    and the script will try to automatically unpack the zipfile, rename
    folders and files to use b-numbers instead of clear names, shuffle
    participants, run the submitted jupyter notebooks to make sure they
    aren't obviously broken and then pack each submission as zip file
    ready for re-upload.

    To make reviews easier, notebook files are also (additionally)
    converted to pdf and html and added to the upload zip.

3. **Deal with issues**

    Each submission is smoke-tested to catch obvious problems. In case
    issues are found an email is prepared to contact the student in
    question with an error log attached. Typical problems are missing
    files, missing notebooks, and errors rising from out-of-order
    execution which can be quickly addressed if caught early. It is
    recommended to test-run this script before the deadline to allow
    students to fix their submissions.

3. **Upload for review**

    Log back into OLAT and upload each solution for review. Since there
    is no review step in OLAT, reject students solution and upload the
    corresponding zip file. This needs to be clearly communicated to
    students and will still lead to confusion. Students can then
    download the uploaded zip and conduct and upload a review as an
    revision in OLAT.
