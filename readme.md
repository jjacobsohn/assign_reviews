# Reassign students solutions for review

This is a helper script to unpack, obfuscate, shuffle, test, export and pack OLAT submissions for review
originally developed for the project "JUNOSOL - Jupyter-notebooks for self-organized learning".

Requirements were:

- mostly automated
- should capture obvious problems early
- should try to obfuscate participants for semi-blind reviews
- handle malformed zips produced by OLAT and mangled submissions

More on the general workflow see [howto/howto.md](howto/howto.md)

Setup:

    conda env create -f environment.yml -p ./env
    conda activate ./env
    
Usage:

    python assign_reviews.py -h

